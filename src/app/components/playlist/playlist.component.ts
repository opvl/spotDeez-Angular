import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { LocalPlaylistService } from '../../services/local-playlist.service';
import { ActivatedRoute } from '@angular/router';

import { Playlist } from '../../models/playlist';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {

  playlist;
  id: string;

  // tslint:disable-next-line: variable-name
  constructor(private _spotifyService: LocalPlaylistService, private _route: ActivatedRoute) { }

  ngOnInit() {
    // Get the ID from the URL string i.e localhost:4200/playlist/id/gf172vbd7asyfvduv
    // gf172vbd7asyfvduv is accessed by params.id
    // console.log(this._route.params);
    // this._route.params.subscribe(params => {
    //   console.log(params.id + this.id);
    //   this.id = params.id;
    //   this._spotifyService.getAuth().subscribe(res => {
    //     this._spotifyService.getPlaylist(this.id, res.access_token).subscribe(playlist => {
    //       this.playlist = playlist;
    //     });
    //   });
    // });
    this.playlist = this._spotifyService.getPlaylist();
  }
}
