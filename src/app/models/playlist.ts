import { Author } from './author';
import { Track } from './track';
import { CoverImage } from './cover-image';

export interface Playlist {
    name: string;
    author: Author;
    tracks: Track[];
    description: string;
    followers: number;
    coverImage: CoverImage;
    // spotify playlist id, same as last part of playlist uri
    uri: string;
}
