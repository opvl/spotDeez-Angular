export interface CoverImage {
    // array of image urls, for use on playlists as well as albums
    parts: string[];
}
