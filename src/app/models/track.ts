import { Album } from './album';
import { Artist } from './artist';

export interface Track {
    name: string;
    album: Album;
    artist: Artist;
    id: number;
    // look at spotify documentation for clarification on this, unsure of tracklength format
    length: string;
    // spotify track id, same as last part of track uri
    uri: string;
}
