export interface Artist {
    name: string;
    // spotify artist ID, same as last part of URI
    uri: string;
}
