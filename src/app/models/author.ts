export interface Author {
    name: string;
    // spotify user ID, same as last part of URI
    uri: string;
}
