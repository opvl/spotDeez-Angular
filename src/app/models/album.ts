import { Artist } from './artist';

export interface Album {
    name: string;
    artist: Artist;
    // spotify album id, same as last part of URI
    uri: string;
}
