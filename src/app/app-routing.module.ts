import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';

import { PlaylistComponent } from './components/playlist/playlist.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';

// Decalre all routes in here
const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'playlist/:id',
    component: PlaylistComponent
  },
  /*{
    path: 'album/:id',
    component: AlbumComponent
  }*/
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
