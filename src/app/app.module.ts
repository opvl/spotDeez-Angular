import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

/* Import Components */
import { NavMainComponent } from './components/nav-main/nav-main.component';
import { FooterMainComponent } from './components/footer-main/footer-main.component';

import { SpotifyService } from './services/spotify.service';
import { LocalPlaylistService } from './services/local-playlist.service';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMainComponent,
    FooterMainComponent,
    PlaylistComponent,
    HomeComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    SpotifyService,
    LocalPlaylistService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
