import { TestBed } from '@angular/core/testing';

import { LocalPlaylistService } from './local-playlist.service';

describe('LocalPlaylistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalPlaylistService = TestBed.get(LocalPlaylistService);
    expect(service).toBeTruthy();
  });
});
