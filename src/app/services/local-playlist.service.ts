import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocalPlaylistService {
  private playListUrl = '../../assets/temp/playlist-loc.json';
  private playListJson = {
    collaborative: false,
    description: 'Raspberry Test',
    external_urls: {
      spotify: 'https://open.spotify.com/playlist/61HwSk4S2zf8BttYD6ztaz'
    },
    followers: {
      href: null,
      total: 0
    },
    href: 'https://api.spotify.com/v1/playlists/61HwSk4S2zf8BttYD6ztaz',
    id: '61HwSk4S2zf8BttYD6ztaz',
    images: [{
      height: 640,
      url: 'https://i.scdn.co/image/9ff65a49173e00e39e1b93875c198c8af0bae3ea',
      width: 640
    }],
    name: 'rasp-test',
    owner: {
      display_name: 'opalbeats',
      external_urls: {
        spotify: 'https://open.spotify.com/user/opalbeats'
      },
      href: 'https://api.spotify.com/v1/users/opalbeats',
      id: 'opalbeats',
      type: 'user',
      uri: 'spotify:user:opalbeats'
    },
    primary_color: null,
    public: true,
    snapshot_id: 'Miw2NjAzMGExY2Q3NDdiMWFmNjcwOTcyZjJhYzZjM2U0ZWYyMWMxNmY1',
    tracks: {
      href: 'https://api.spotify.com/v1/playlists/61HwSk4S2zf8BttYD6ztaz/tracks?offset=0&limit=100&market=GB',
      items: [{
        added_at: '2019-03-05T15:56:32Z',
        added_by: {
          external_urls: {
            spotify: 'https://open.spotify.com/user/opalbeats'
          },
          href: 'https://api.spotify.com/v1/users/opalbeats',
          id: 'opalbeats',
          type: 'user',
          uri: 'spotify:user:opalbeats'
        },
        is_local: false,
        primary_color: null,
        track: {
          album: {
            album_type: 'album',
            artists: [{
              external_urls: {
                spotify: 'https://open.spotify.com/artist/0PFtn5NtBbbUNbU9EAmIWF'
              },
              href: 'https://api.spotify.com/v1/artists/0PFtn5NtBbbUNbU9EAmIWF',
              id: '0PFtn5NtBbbUNbU9EAmIWF',
              name: 'Toto',
              type: 'artist',
              uri: 'spotify:artist:0PFtn5NtBbbUNbU9EAmIWF'
            }],
            external_urls: {
              spotify: 'https://open.spotify.com/album/6euMPZz8wRBQBf9U2W91Xw'
            },
            href: 'https://api.spotify.com/v1/albums/6euMPZz8wRBQBf9U2W91Xw',
            id: '6euMPZz8wRBQBf9U2W91Xw',
            images: [{
              height: 640,
              url: 'https://i.scdn.co/image/9ff65a49173e00e39e1b93875c198c8af0bae3ea',
              width: 640
            },
            {
              height: 300,
              url: 'https://i.scdn.co/image/4ddffa7ee1de6c0599bb3ca1f6b2e9a5e9bb872e',
              width: 300
            },
            {
              height: 64,
              url: 'https://i.scdn.co/image/63c0b68ac53c1de25fa091ca48f79ddd534ce506',
              width: 64
            }
            ],
            name: 'Tambu',
            release_date: '1995-05',
            release_date_precision: 'month',
            total_tracks: 13,
            type: 'album',
            uri: 'spotify:album:6euMPZz8wRBQBf9U2W91Xw'
          },
          artists: [{
            external_urls: {
              spotify: 'https://open.spotify.com/artist/0PFtn5NtBbbUNbU9EAmIWF'
            },
            href: 'https://api.spotify.com/v1/artists/0PFtn5NtBbbUNbU9EAmIWF',
            id: '0PFtn5NtBbbUNbU9EAmIWF',
            name: 'Toto',
            type: 'artist',
            uri: 'spotify:artist:0PFtn5NtBbbUNbU9EAmIWF'
          }],
          disc_number: 1,
          duration_ms: 366373,
          episode: false,
          explicit: false,
          external_ids: {
            isrc: 'USSM19502705'
          },
          external_urls: {
            spotify: 'https://open.spotify.com/track/1MzFmVDSl7Vm00f3Hw4Xfx'
          },
          href: 'https://api.spotify.com/v1/tracks/1MzFmVDSl7Vm00f3Hw4Xfx',
          id: '1MzFmVDSl7Vm00f3Hw4Xfx',
          is_local: false,
          is_playable: true,
          name: 'I Will Remember',
          popularity: 53,
          preview_url: 'https://p.scdn.co/mp3-preview/673e5dcd6cee99d3027ff86a59743936d52c8a68?cid=774b29d4f13844c495f206cafdad9c86',
          track: true,
          track_number: 2,
          type: 'track',
          uri: 'spotify:track:1MzFmVDSl7Vm00f3Hw4Xfx'
        },
        video_thumbnail: {
          url: null
        }
      },
      {
        added_at: '2019-03-05T15:56:32Z',
        added_by: {
          external_urls: {
            spotify: 'https://open.spotify.com/user/opalbeats'
          },
          href: 'https://api.spotify.com/v1/users/opalbeats',
          id: 'opalbeats',
          type: 'user',
          uri: 'spotify:user:opalbeats'
        },
        is_local: false,
        primary_color: null,
        track: {
          album: {
            album_type: 'album',
            artists: [{
              external_urls: {
                spotify: 'https://open.spotify.com/artist/0PFtn5NtBbbUNbU9EAmIWF'
              },
              href: 'https://api.spotify.com/v1/artists/0PFtn5NtBbbUNbU9EAmIWF',
              id: '0PFtn5NtBbbUNbU9EAmIWF',
              name: 'Toto',
              type: 'artist',
              uri: 'spotify:artist:0PFtn5NtBbbUNbU9EAmIWF'
            }],
            external_urls: {
              spotify: 'https://open.spotify.com/album/6euMPZz8wRBQBf9U2W91Xw'
            },
            href: 'https://api.spotify.com/v1/albums/6euMPZz8wRBQBf9U2W91Xw',
            id: '6euMPZz8wRBQBf9U2W91Xw',
            images: [{
              height: 640,
              url: 'https://i.scdn.co/image/9ff65a49173e00e39e1b93875c198c8af0bae3ea',
              width: 640
            },
            {
              height: 300,
              url: 'https://i.scdn.co/image/4ddffa7ee1de6c0599bb3ca1f6b2e9a5e9bb872e',
              width: 300
            },
            {
              height: 64,
              url: 'https://i.scdn.co/image/63c0b68ac53c1de25fa091ca48f79ddd534ce506',
              width: 64
            }
            ],
            name: 'Tambu',
            release_date: '1995-05',
            release_date_precision: 'month',
            total_tracks: 13,
            type: 'album',
            uri: 'spotify:album:6euMPZz8wRBQBf9U2W91Xw'
          },
          artists: [{
            external_urls: {
              spotify: 'https://open.spotify.com/artist/0PFtn5NtBbbUNbU9EAmIWF'
            },
            href: 'https://api.spotify.com/v1/artists/0PFtn5NtBbbUNbU9EAmIWF',
            id: '0PFtn5NtBbbUNbU9EAmIWF',
            name: 'Toto',
            type: 'artist',
            uri: 'spotify:artist:0PFtn5NtBbbUNbU9EAmIWF'
          }],
          disc_number: 1,
          duration_ms: 366373,
          episode: false,
          explicit: false,
          external_ids: {
            isrc: 'USSM19502705'
          },
          external_urls: {
            spotify: 'https://open.spotify.com/track/1MzFmVDSl7Vm00f3Hw4Xfx'
          },
          href: 'https://api.spotify.com/v1/tracks/1MzFmVDSl7Vm00f3Hw4Xfx',
          id: '1MzFmVDSl7Vm00f3Hw4Xfx',
          is_local: false,
          is_playable: true,
          name: 'I Will Remember',
          popularity: 53,
          preview_url: 'https://p.scdn.co/mp3-preview/673e5dcd6cee99d3027ff86a59743936d52c8a68?cid=774b29d4f13844c495f206cafdad9c86',
          track: true,
          track_number: 2,
          type: 'track',
          uri: 'spotify:track:1MzFmVDSl7Vm00f3Hw4Xfx'
        },
        video_thumbnail: {
          url: null
        }
      }],
      limit: 100,
      next: null,
      offset: 0,
      previous: null,
      total: 1
    },
    type: 'playlist',
    uri: 'spotify:playlist:61HwSk4S2zf8BttYD6ztaz'
  };

  // tslint:disable-next-line: variable-name
  constructor(private _http: HttpClient) { }

  // Get data about playlist that has been chosen to view
  getPlaylist() {
    return this.playListJson;
  }
}
