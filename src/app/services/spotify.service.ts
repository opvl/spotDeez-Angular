/* Credit for the Angular implementation of Spotify API:
        Gamil Pierre
 (https://github.com/Pierre-D-G)
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class SpotifyService {
  private searchUrl: string;
  private artistUrl: string;
  private albumsUrl: string;
  private albumUrl: string;
  private playlistUrl: string;
  private clientId: string = environment.clientId;
  private clientSecret: string = environment.clientSecret;
  private proxy = 'https://cors-anywhere.herokuapp.com/';
  private body: any;


  // tslint:disable-next-line: variable-name
  constructor(private _http: HttpClient) { }


  // Get access token from Spotify to use API
  getAuth = () => {

    const headers = new HttpHeaders();
    headers.append('Authorization', 'Basic ' + (this.clientId + ':' + this.clientSecret));
    headers.append('Content-Type', 'application/x-www-form-urlencoded'); // application/x-www-form-urlencoded

    const params: URLSearchParams = new URLSearchParams();
    params.set('grant_type', 'client_credentials');
    const body = params.toString();

    return this._http.post<any>('https://accounts.spotify.com/api/token', body, { headers });
  }

  // Get search results for a query
  searchMusic(query: string, type = 'artist', authToken: string) {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + authToken);

    this.searchUrl = 'https://api.spotify.com/v1/search?query=' + query + '&offset=0&limit=20&type=' + type + '&market=US';

    return this._http.get<JSON>(this.searchUrl, { headers });
  }

  // Get data about artist that has been chosen to view
  getArtist(id: string, authToken: string) {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + authToken);

    this.artistUrl = 'https://api.spotify.com/v1/artists/' + id;

    return this._http.get<JSON>(this.artistUrl, { headers });
  }

  // Get the albums about the artist that has been chosen
  getAlbums(id: string, authToken: string) {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + authToken);

    this.albumsUrl = 'https://api.spotify.com/v1/artists/' + id + '/albums?market=US&album_type=single';

    return this._http.get<JSON>(this.albumsUrl, { headers });
  }

  // Get Tracks in ablum selected
  getAlbum(id: string, authToken: string) {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + authToken);

    this.albumUrl = 'https://api.spotify.com/v1/albums/' + id;

    return this._http.get<JSON>(this.albumUrl, { headers });
  }

  // Get data about playlist that has been chosen to view
  getPlaylist(id: string, authToken: string) {
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Bearer ' + authToken);

    this.playlistUrl = 'https://api.spotify.com/v1/playlists/' + id;

    return this._http.get<JSON>(this.playlistUrl, { headers });
  }
}
